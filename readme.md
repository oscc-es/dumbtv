# Video (and podcast) RSS Reader for my _dumbTV_

Homepage for [Óscar García](https://www.ogarcia.es)'s _dumbTV_'s browser with latest TV shows, videos and podcasts I like.

Check it out at [**www.dumbtv.es**](https://www.dumbtv.es)

Highly inspired by [Stephanie Eckles](https://thinkdobecreate.com/)'s [Eleventy RSS Reader Starter](https://github.com/5t3ph/eleventy-rss-reader/) on top of my [oscc-base-no-blog 11ty template](https://gitlab.com/oscc-es/oscc-base-no-blog).

## Technical features

The `.json` files which have the channels' info are located at `src/feeds/`

- Youtube channels (ID can be found as _browse_id_ on channel's source code https://www.youtube.com/feeds/videos.xml?channel_id=CHANNEL_ID)
- Twitch channels (uses Laszlo Zeke's [Twitch RSS Webapp](https://github.com/lzeke0/TwitchRSS))
- RTVE play TV shows (uses [RTVE-API no oficial docs](https://ulisesgascon.github.io/RTVE-API/) by [Ulises Gascón](https://twitter.com/kom_256))
- Youtube 'nocookie' links
- Filter videos by name
- Exclude videos by name
- Filter latest videos within an amount of days
- Filter Youtube videos by quality based on average views per day
- Filter RTVE play videos by quality based on imdb rating (uses [The Open Movie Database](https://www.omdbapi.com/)) (env variable called `OMDB_API_KEY`)
- Thumbnails are local stored and optimised
- Links to channel's video platforms
- Links to channel's RSS
- Quick links to mainstream video platforms
- Ability to mark videos as viewed
- Choose regular thumbnail or cover thumbnail
- Podcasts RSS

## Getting started

- Run `npm install`
- Run `npm start` to run locally
- Run `npm run production` to do a prod build
- Run `npm run release` to generate/update the changelog file and increment the corresponding npm version on package.json

## Cron task

- Daily Cron task to build the site once a day to update the list of videos and podcasts.
