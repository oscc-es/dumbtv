import Content from './components/content.js';

class App {
  constructor() {
    this.contentInstance = new Content(
      new URL(import.meta.url).searchParams.get('feedToShow')
    );
    this.dtvInstances = document.querySelectorAll('[data-element="dtv"]');
    this.itemLinks = document.querySelectorAll('[data-item-link]');

    // Show the checkboxes now that the JS is ready
    this.dtvInstances.forEach(item => item.removeAttribute('hidden'));

    // Run the initial content application and the listener
    this.applyContent();
    this.cleanLocalStorage();
    this.listen();
  }

  listen() {
    // Listens for checkboxes changes and applies to local storage accordingly
    this.dtvInstances.forEach(dtv => {
      dtv.addEventListener('change', () => {
        this.contentInstance.save(dtv.getAttribute('data-id'), dtv.checked);
      });
    });

    // Listens for video links and applies to local storage when clicked
    this.itemLinks.forEach(dtv => {
      dtv.addEventListener('click', evt => {
        this.contentInstance.save(dtv.getAttribute('href'), true);

        this.applyContent();
      });
    });

    // If content changes in another tab, sync it up
    window.addEventListener('storage', evt => {
      const {isTrusted} = evt;

      if (!isTrusted) {
        return;
      }

      this.applyContent();
    });
  }

  // Loads the content from the content interface and applies it
  applyContent() {
    this.existingContent = [];

    this.dtvInstances.forEach(dtv => {
      const id = dtv.getAttribute('data-id');
      dtv.checked = this.contentInstance.load(id);
      this.existingContent.push(id);
    });
  }

  cleanLocalStorage() {
    this.contentInstance.list().forEach(item => {
      if (!this.existingContent.includes(item)) {
        this.contentInstance.delete(item);
      }
    });
  }
}

const appInstance = new App();
