/**
 * An interface for interacting with local storage
 */
class Content {
  constructor(feedToShow = false) {
    this.load();
    this.key = 'dumbtv' + window.location.pathname;

    this.key = 'dumbtv' + (feedToShow ? `/${feedToShow}/` : window.location.pathname);
  }

  load(id = 'default') {
    let existingContent = localStorage.getItem(this.key);

    this.content = '';

    if (existingContent) {
      try {
        existingContent = JSON.parse(existingContent);
        this.content = existingContent[id] ? existingContent[id] : '';
      } catch (ex) {}
    }

    return this.content;
  }

  save(id, content) {
    let existingContent = localStorage.getItem(this.key);

    // If there's existing content we just add/edit the ref
    if (existingContent) {
      try {
        existingContent = JSON.parse(existingContent);
        existingContent[id] = content;
        localStorage.setItem(this.key, JSON.stringify(existingContent));
      } catch (ex) {
        return;
      }
      return;
    }

    // Nothing in storage yet, so create it all
    const newContent = {};
    newContent[id] = content;
    localStorage.setItem(this.key, JSON.stringify(newContent));
  }

  list() {
    let existingContent = localStorage.getItem(this.key);

    if (existingContent) {
      try {
        return Object.keys(JSON.parse(existingContent));
      } catch (ex) {}
    }

    return [];
  }

  delete(id) {
    let existingContent = localStorage.getItem(this.key);

    // If there's existing content we just add/edit the ref
    if (existingContent) {
      try {
        existingContent = JSON.parse(existingContent);
        delete existingContent[id];
        localStorage.setItem(this.key, JSON.stringify(existingContent));
      } catch (ex) {}
    }

    return;
  }
}

export default Content;
