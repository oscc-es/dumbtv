// Optimal size for large original images: 1920px / 2560px

const fs = require('fs');
const path = require('path');
const Image = require('@11ty/eleventy-img');

const imgOptions = {
  formats: ['avif', 'webp', null],
  widths: [285, 480, 640], //480 it's the smallest thumbnail, Youtube, but Rtve aspect ratio needs larger width to achieve same height as Youtube
  urlPath: '/images/',
  inputDir: null,
  outputDir: './dist/images/',
  cacheOptions: {
    duration: '1w'
  }
};

// this logic it's not used
module.exports = function (eleventyConfig) {
  eleventyConfig.on('eleventy.before', () => {
    if (!fs.existsSync(imgOptions.inputDir)) {
      throw new Error(
        `Image input directory to optimise does not exist: ${imgOptions.inputDir}`
      );
    }

    fs.readdir(imgOptions.inputDir, (err, files) => {
      if (files.length <= 0) {
        return;
      }

      files.forEach(fileName => {
        if (
          !(
            fileName.endsWith('.png') ||
            fileName.endsWith('.jpg') ||
            fileName.endsWith('.jpeg')
          )
        ) {
          return;
        }

        if (
          fs.existsSync(
            path.resolve(
              __dirname,
              `${imgOptions.outputDir}${fileName.split('.').shift()}-${
                imgOptions.widths[0]
              }w.${imgOptions.formats[0]}`
            )
          )
        ) {
          return;
        }

        Image(`src/images/src/${fileName}`, imgOptions);
      });
    });
  });
};

module.exports.optImgOptions = imgOptions;
