const Origin = require('./Origin');
const parseString = require('xml2js').parseString;

class Podcast extends Origin {
  constructor(data) {
    super(data);
  }

  parseFeedData(response) {
    parseString(response, (err, result) => (this.data = result));

    this.siteTitle = this.data.rss.channel[0].title[0];

    if (this.data.rss.channel[0].item == undefined) {
      return [];
    }

    return this.data.rss.channel[0].item.map(item => ({
      isPodcast: this.isPodcast,
      siteTitle: this.siteTitle,
      siteUrl: this.siteUrl,
      feedUrl: this.feedUrl,
      title: item.title[0],
      url: item.enclosure[0]['$'].url,
      date: new Date(item.pubDate[0]),
      thumbnail: item['itunes:image']
        ? item['itunes:image'][0]['$'].href
        : this.data.rss.channel[0].image[0].url[0],
      cover: this.cover,
      quality: 0
    }));
  }
}

module.exports = Podcast;
