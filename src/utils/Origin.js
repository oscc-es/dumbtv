const EleventyFetch = require('@11ty/eleventy-fetch');

class Origin {
  constructor(data) {
    this.dataType = data.type || 'text';
    this.feedPeriod = data.feedPeriod || 7; //days
    this.filter = data.filter || false;
    this.exclude = data.exclude || false;
    this.quality = data.quality || 0;
    this.isPodcast = data.isPodcast || 0;
    this.feedUrl = data.feed;
    this.siteUrl = data.url;
    this.cover = data.cover || false;
    this.data;

    const d = new Date();
    this.periodDate = new Date(d.setDate(d.getDate() - this.feedPeriod)).getTime();
  }

  get items() {
    return this.parseFeed().then(async response =>
      (await this.parseFeedData(response)).filter(
        entry =>
          this.filterLatestFeed(entry) &&
          this.filterEntries(entry) &&
          this.filterExclude(entry) &&
          this.filterQuality(entry)
      )
    );
  }

  parseFeed() {
    return EleventyFetch(this.feedUrl, {
      duration: '2h', // save for 2 hours
      type: this.dataType
    }).catch(() => {
      return Promise.resolve([]);
    });
  }

  parseFeedData(response) {
    return response;
  }

  filterLatestFeed(entry) {
    return new Date(entry.date).getTime() > this.periodDate;
  }

  filterEntries(entry) {
    if (!this.filter) {
      return true;
    }

    if (!entry.title) {
      return false;
    }

    if (!Array.isArray(this.filter)) {
      return entry.title.indexOf(this.filter) !== -1;
    }

    for (const key in this.filter) {
      if (entry.title.indexOf(this.filter[key]) === -1) return false;
    }

    return true;
  }

  filterExclude(entry) {
    if (!this.exclude) {
      return true;
    }

    if (!entry.title) {
      return true;
    }

    if (!Array.isArray(this.exclude)) {
      return entry.title.indexOf(this.exclude) === -1;
    }

    for (const key in this.exclude) {
      if (entry.title.indexOf(this.exclude[key]) !== -1) return false;
    }

    return true;
  }

  filterQuality(entry) {
    return entry.quality >= this.quality;
  }
}

module.exports = Origin;
