const Origin = require('./Origin');
const parseString = require('xml2js').parseString;

class Twitch extends Origin {
  constructor(data) {
    super(data);
  }

  parseFeedData(response) {
    parseString(response, (err, result) => (this.data = result));
    this.siteTitle = (this.data.rss.channel[0].title + '').replace(
      "'s Twitch video RSS",
      ''
    );

    const entries = this.data.rss.channel[0].item;
    const regex = /src\s*=\s*"(.+?)"/;

    return entries.map(entry => ({
      isPodcast: this.isPodcast,
      siteTitle: this.siteTitle,
      siteUrl: this.siteUrl,
      feedUrl: this.feedUrl,
      title: entry.title[0],
      url: `https://player.twitch.tv/?autoplay=true&muted=false&parent=example.com&video=${entry.guid[0]['_']}`,
      date: new Date(entry.pubDate[0]),
      thumbnail: entry.description[0].match(regex)[1],
      cover: this.cover,
      quality: 0
    }));
  }
}

module.exports = Twitch;
