const Origin = require('./Origin');
const parseString = require('xml2js').parseString;

class Youtube extends Origin {
  constructor(data) {
    super(data);
    this.maxAvgViewsPerDay = 0;
  }

  parseFeedData(response) {
    parseString(response, (err, result) => (this.data = result));

    this.siteTitle = this.data.feed.title[0];

    if (this.data.feed.entry == undefined) {
      return [];
    }

    const entries = this.data.feed.entry.filter(entry => {
      // take advantage of this loop to calculate entry with max average of views per day
      const avgViewsPerDay = this.getAvgViewsPerDay(entry);
      this.maxAvgViewsPerDay =
        avgViewsPerDay > this.maxAvgViewsPerDay ? avgViewsPerDay : this.maxAvgViewsPerDay;

      // This filter avoids videos with less than 5 views assuming that those are pre-releases
      const entryViews =
        entry['media:group'][0]['media:community'][0]['media:statistics'][0]['$'][
          'views'
        ];
      return entryViews > 5;
    });

    return entries.map(entry => ({
      isPodcast: this.isPodcast,
      siteTitle: this.siteTitle,
      siteUrl: this.siteUrl,
      feedUrl: this.feedUrl,
      title: entry.title[0],
      url: `https://www.youtube-nocookie.com/embed/${entry['yt:videoId'][0]}`,
      date: new Date(entry.published[0]),
      thumbnail: entry['media:group'][0]['media:thumbnail'][0]['$'].url,
      cover: this.cover,
      quality: this.quality ? this.getEntryQuality(entry) : 0
    }));
  }

  getAvgViewsPerDay(entry) {
    const entryViews =
      entry['media:group'][0]['media:community'][0]['media:statistics'][0]['$']['views'];
    const numberOfDays = this.getNumberOfDaysFrom(new Date(entry.published[0])) || 1;
    return entryViews / numberOfDays;
  }

  getEntryQuality(entry) {
    const avgViewsPerDay = this.getAvgViewsPerDay(entry);

    return Math.round((avgViewsPerDay / this.maxAvgViewsPerDay) * 100);
  }

  getNumberOfDaysFrom(date) {
    const now = new Date();
    const oneDay = 1000 * 60 * 60 * 24;

    const diffInTime = now.getTime() - date.getTime();
    const diffInDays = Math.round(diffInTime / oneDay);

    return diffInDays;
  }
}

module.exports = Youtube;
