const fastglob = require('fast-glob');
const fs = require('fs');

const Youtube = require('../utils/Youtube.js');
const Twitch = require('../utils/Twitch.js');
const Rtve = require('../utils/Rtve.js');
const Podcast = require('../utils/Podcast.js');

// https://github.com/5t3ph/eleventy-rss-reader/blob/main/src/_data/feeds.js
const getSources = async () => {
  // Create a "glob" of all feed json files
  const feedFiles = await fastglob('./src/feeds/*.json', {
    caseSensitiveMatch: false
  });

  // Loop through those files and add their content to our `feeds` Set
  let feeds = [];
  for (let feed of feedFiles) {
    const feedData = JSON.parse(fs.readFileSync(feed));
    feeds.push(feedData);
  }

  // Return the feeds Set of objects within an array
  return feeds;
};

const getItems = async origins => {
  return Promise.all(origins.map(async origin => await origin.items));
};

const getFeed = async rawOrigins => {
  const youtubeOrigins = rawOrigins.youtube.map(item => new Youtube(item));
  const twitchOrigins = rawOrigins.twitch.map(item => new Twitch(item));
  const rtveOrigins = rawOrigins.rtve.map(item => new Rtve(item));
  const podcastOrigins = rawOrigins.podcast.map(item => new Podcast(item));

  const origins = [
    ...youtubeOrigins,
    ...twitchOrigins,
    ...rtveOrigins,
    ...podcastOrigins
  ];

  const groupedItems = await getItems(origins);

  const items = groupedItems.flat();

  return items.sort((a, b) => (a.date > b.date ? -1 : 1));
};

module.exports = async () => {
  const feedSources = await getSources();

  const feeds = [];
  for (const channel of feedSources) {
    channel.feed = await getFeed(channel.rawOrigins);

    feeds.push(channel);
  }

  return feeds;
};
