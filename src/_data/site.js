const segment = () => (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
const random = `${segment()}-${segment()}-${segment()}`;

module.exports = {
  url: process.env.URL || 'http://localhost:8080',
  versionHash: random
};
