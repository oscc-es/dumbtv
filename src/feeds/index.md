---
pagination:
  data: feeds
  size: 1
  alias: channel
  addAllPagesToCollections: true
permalink: '/{{ channel.title | slugify }}/'
layout: layouts/home.njk
changeFreq: daily
eleventyComputed:
  title: '{{ channel.title }}'
  subtitle: '{{ channel.title }}'
  metaDesc: '{{ channel.metaDesc }}'
  lang: '{{ channel.lang }}'
---

{{ channel.description | safe }}
