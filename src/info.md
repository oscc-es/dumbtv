---
title: 'Información'
metaDesc: '¿Qué es dumbTV? ¿qué tipos de canales y programas están disponibles? descubre la información relativa a este proyecto desarrollado por Óscar García.'
layout: 'layouts/page.njk'
---

[**_dumbTV_**](/) es un lector de feeds de podcasts y vídeo de _Youtube_, _Twitch_ y _RTVE play_ [desarrollado por Óscar García](https://www.ogarcia.es) que permite agrupar el contenido de los canales y programas de televisión que prefieras en un solo sitio y de manera personalizada.

## Motivación de dumbTV

Tengo un portátil viejo conectado a una televisión que **no es _smartTV_** y quería una página de inicio para mi explorador desde la que acceder a todos los contenidos de entretenimiento que suelo visitar.

Como usuario de internet tengo mis manías: aprecio mi privacidad, no acostumbro a loguearme en los sitios a los que puedo acceder sin necesidad de hacerlo ni tampoco suelo instalarme aplicaciones cuando la versión web de un sitio ya me ofrece lo que necesito. Esta web me permite consumir vídeos con cierta comodidad bajo el paraguas de la configuración de mi explorador y sin renunciar a mis preferencias de navegación.

Aclarar, por supuesto, que esta web no recoge datos de sus usuarios y los vídeos se alojan y se visualizan en las plataformas referenciadas.

## Características de dumbTV

Algunas de las características de [**_dumbTV_**](/):

- Los links a los vídeos de _Youtube_ son a su versión sin cookies.
- Permite filtrar los vídeos de un feed que contengan un texto, o textos, determinado(s) en su título.
- Permite excluir los vídeos de un feed que contengan un texto, o textos, determinado(s) en su título.
- Permite mostrar solo los últimos vídeos publicados de un feed dentro de un periodo de tiempo definido en días.
- Muestra links a plataformas de series y películas.
- Permite marcar vídeos como vistos / no vistos
- Permite filtrar vídeos de _Youtube_ por calidad (según la media de visualizaciones por día)
- Permite filtrar vídeos de _RTVE play_ por calidad (según su puntuación en _imdb_)
- Elige entre mostrar miniatura del vídeo or portada si está disponible.
- Acepta RSS de podcasts

## Próximos pasos

Voy incorporando ideas de manera más o menos ordenada. Aquí algunas que no quiero que se me olviden pero tampoco se si las acabaré incorporando ni cuando:

- Poder archivar los vídeos que me gusten.
- Avisar cuando un canal de Twitch esté en directo.
- Permitir que cualquier usuario pueda montar y personalizar su propio canal.

Puedes echar un ojo al código fuente en el [repositorio de dumbTV](https://gitlab.com/oscc-es/dumbtv) y avisar de errores o sugerencias. También puedes utilizar este mail [oscar@dumbtv.es](mailto:oscar@dumbtv.es).

## Créditos

Desde el punto de vista técnico la aplicación está inspirada en el código de [_Eleventy RSS Reader Starter_](https://github.com/5t3ph/eleventy-rss-reader/) de [_Stephanie Eckles_](https://front-end.social/@5t3ph).

El acceso a los datos de Twitch se hace utilizando [_Twitch RSS Webapp_](https://github.com/lzeke0/TwitchRSS) de _Laszlo Zeke_.

El acceso a los datos de _RTVE play_ fue especialmente fácil gracias a [_RTVE-API no oficial docs_](https://ulisesgascon.github.io/RTVE-API/) de [_Ulises Gascón_](https://twitter.com/kom_256).

El acceso a las puntuaciones de _imdb_ se hace utilizando [The Open Movie Database](https://www.omdbapi.com/).

La tipografía utilizada en el logo, [_SKWAR_](https://heydon.works/skwar/), es obra de [Heydon](https://front-end.social/@heydon) cuyo trabajo en general junto al de [Andy Bell](https://mastodon.social/@belldotbz) está influyendo intensamente en todo lo que hago.

## Si te gusta dumbTV

Si visitas a menudo esta página, te gusta o te parece útil, considera hacérmelo saber mediante un email a [oscar@dumbtv.es](mailto:oscar@dumbtv.es) o un mensaje en [mi Mastodon](https://paquita.masto.host/@ogarcia). Siempre motiva saber que tu trabajo llega a alguien.

Si quieres contribuir a que tenga algo más de tiempo para mejorar esta web o desarrollar otras nuevas, puedes [invitarme a un café](https://ko-fi.com/ogarcia).
