// Can be used within Nunjucks template or markdown if Nunjucks is the default markdownTemplateEngine
// {% img src="/images/src/bunny-lake-version.png", title="Captura de la página de inicio inspirada en los títulos de crédito de Bunny Lake Is Missing.", alt="Papel oscuro rasgado que deja ver un papel blanco debajo donde se puede leer 'Óscar García presenta'", loading="eager", figure=true %}

const Image = require('@11ty/eleventy-img');
const {optImgOptions} = require('../plugins/optimize-img-formats.js');

const breakpoints = ['(max-width: 300px)', '(max-width: 400px)', '(max-width: 640px)'];

module.exports = async function imageShortcode(customOptions) {
  if (
    !customOptions.src ||
    customOptions.src.startsWith('https://vod-secure.twitch.tv/_404/')
  ) {
    customOptions.src = 'https://vod-secure.twitch.tv/_404/404_processing_320x180.png';
  }

  const stats = await Image(customOptions.src, optImgOptions);

  const formats = ['jpeg', 'png', 'webp', 'avif'];
  const originalFormatMeta = formats.filter(format => !stats[format]);

  const sizesValueComponents = originalFormatMeta.map((current, index) => {
    if (index < originalFormatMeta.length - 1) {
      return `${breakpoints[index]} ${current.width}px`;
    }

    return `${current.width}px`;
  });

  const defaultOptions = {
    alt: '',
    sizes: sizesValueComponents.join(', '),
    loading: 'lazy',
    decoding: 'async'
  };

  const options = {
    ...defaultOptions,
    ...customOptions
  };

  const isFigure = options.figure ? delete options.figure : false;
  delete options.__keywords;

  const pictureHTML = Image.generateHTML(stats, options, {whitespaceMode: 'block'});

  if (!isFigure) {
    return pictureHTML;
  }

  if (!options.title) {
    return `<figure>${pictureHTML}</figure>`;
  }

  return `<figure>${pictureHTML}<figcaption>${options.title}</figcaption></figure>`;
};
